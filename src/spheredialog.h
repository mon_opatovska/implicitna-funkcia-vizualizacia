﻿#pragma once
#include <QDialog>
#include "tvarodialog.h"

#include <vtkSmartPointer.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkTriangleFilter.h>

namespace Ui {class SphereDialog;}

class SphereDialog : public TvaroDialog {
	Q_OBJECT

public:
	SphereDialog(QDialog * parent = Q_NULLPTR);
	vtkSmartPointer<vtkActor> GetActor();
	~SphereDialog();

private:
	Ui::SphereDialog *ui;
};
