﻿#include "implicitdialog.h"

ImplicitDialog::ImplicitDialog(QDialog * parent) : TvaroDialog(parent) {
	ui = new Ui::implicitdialog();
	ui->setupUi(this);
}

vtkSmartPointer<vtkActor> ImplicitDialog::GetActor()
{
	vtkSmartPointer<vtkImageData> data = vtkSmartPointer<vtkImageData>::New();
	int xmax = 512, ymax = 512, zmax = 512;
	data->SetExtent(0, xmax - 1, 0, ymax - 1, 0, zmax - 1);
	data->AllocateScalars(VTK_FLOAT, 1);
	data->SetSpacing((ui->spinXend->value() - ui->spinXstart->value()) / 512, 
						(ui->spinYend->value() - ui->spinYstart->value()) / 512,
						(ui->spinZend->value() - ui->spinZstart->value()) / 512);
	data->SetOrigin(ui->spinXstart->value(), ui->spinYstart->value(), ui->spinZstart->value());
	float *dataPtr = (float *)data->GetScalarPointer();
	std::string implF = ui->implLine->text().toStdString();
	
	double bod[3];
	symbol_table_t symbol_table;
	symbol_table.add_variable("x", bod[0]);
	symbol_table.add_variable("y", bod[1]);
	symbol_table.add_variable("z", bod[2]);
	symbol_table.add_constants();

	expression_t expression;
	expression.register_symbol_table(symbol_table);

	parser_t parser;
	if (!parser.compile(implF, expression)) {
		std::cout << parser.error() << std::endl;
		return false;
	}

	double dx = (ui->spinXend->value() - ui->spinXstart->value()) / 512;
	double dy = (ui->spinYend->value() - ui->spinYstart->value()) / 512;
	double dz = (ui->spinZend->value() - ui->spinZstart->value()) / 512;


	//vygenerujeme 3d objem podla funkcie
	for (int i = 0; i < xmax; i++)
	{
		for (int j = 0; j < ymax; j++)
		{
			for (int k = 0; k < zmax; k++)
			{
				bod[0] = ui->spinXstart->value() + dx *i;
				bod[1] = ui->spinYstart->value() + dy *j;
				bod[2] = ui->spinZstart->value() + dz *k;
				
				/*double x, y, z;
				double half = xmax / 2 - 1;
				x = (i - half) / (xmax)* (3.0);
				y = (j - half) / (ymax)* (3.0);
				z = (k - half) / (zmax)* (3.0);*/
				dataPtr[i + xmax * (j + ymax * k)] = expression.value();//pow((2 * x *x + y *y + z *z - 1), 3) - (1 / 10)*x * x* z *z*z - y *y *z *z*z;
			}
		}
	}

	//vytvorime si izoplochu
	vtkSmartPointer<vtkMarchingCubes> surface = vtkSmartPointer<vtkMarchingCubes>::New();
	surface->SetInputData(data.Get());
	surface->ComputeNormalsOn();
	
	//izoplocha hodnoty 0.000001, vtk ma nejake problemy ked to je presna nula...
	surface->SetValue(0, 0.000001);
	
	vtkSmartPointer<vtkPolyDataMapper> mapper =
		vtkSmartPointer<vtkPolyDataMapper>::New();
	mapper->SetInputConnection(surface->GetOutputPort());
	mapper->Update();
	
	actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(mapper);
	vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(mapper->GetInput());

	//odstranime farbu pridanu marching cubes
	polydata->GetPointData()->RemoveArray(0);

	//preskalujeme body na povodne hodnoty
	//double bod[3];
	size_t pocet = polydata->GetNumberOfPoints();
	for (int i = 0; i < pocet; i++)
	{
		polydata->GetPoint(i, bod);
		double x, y, z;
		double half = xmax / 2 - 1;
		x = (bod[0] - half) / (xmax)* (3.0);
		y = (bod[1] - half) / (ymax)* (3.0);
		z = (bod[2] - half) / (zmax)* (3.0);
		polydata->GetPoints()->SetPoint(i, x, y, z);
	}
	polydata->Modified();
	return actor;
}

ImplicitDialog::~ImplicitDialog() {
	delete ui;
	
}
